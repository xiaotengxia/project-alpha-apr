from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.form import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(members=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def detail_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", id=project.id)
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
